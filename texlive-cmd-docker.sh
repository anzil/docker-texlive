#!/bin/sh

CMD=`basename "$0"`
ARG=$@

TEXSRC=${TEXSRC:-$PWD}
TEXADD=${TEXMFHOME:-$HOME/.texmf}
TEXCHE=${TEXMFCACHE:-$HOME/.texcache}

echo "********************************************"
echo "running  : ${CMD}"
echo "arguments: ${ARG}"
echo "********************************************"

docker run -ti --name tex --rm \
   -v ${TEXSRC}:/texsrc:delegated \
   -v ${TEXADD}:/texmf:delegated \
   -v ${TEXCHE}:/texcache:delegated \
   \
   registry.gitlab.com/anzil/docker-texlive \
   \
   /bin/sh -c "texhash /texmf && ${CMD} ${ARG}"
