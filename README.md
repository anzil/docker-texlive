TexLive docker container
=====

This [docker](https://www.docker.com) container helps compiling tex sources without the need to install
[texlive](http://www.tug.org/texlive/) on your system.

Quick setup
-----

Within the cloned git repository execute:
```bash
make
make install
```

Test the docker container and the tex command wrappers with
```bash
make test
```

For subsequent use of the tex wrappers make sure that your local `bin`
directory is in the PATH environment variable, e.g.
```bash
export PATH=~/bin:$PATH
```
This can be made permanent by adding the above line to your `~/.profile` file.

Custom setup
-----

Installation locations can be adjusted via variables, e.g.
```bash
make install TEXMFHOME=/path/to/my/texmf TEXMFCACHE=/path/to/my/texcache TEXBINDIR=/path/to/my/bin
```
while the default values are:
`TEXMFHOME=${HOME}/.texmf`
`TEXMFCACHE=${HOME}/.texcache`
`TEXBINDIR=${HOME}/bin`.


Update
-----

In order to update your (unmodified) repository and installation, simply execute
```bash
git pull && make install
```


More detailed setup explanations
-----

Having docker properly installed, we first have to build our custom-made
docker-texlive image:
```bash
docker build -t registry.gitlab.com/anzil/docker-texlive .
```

Then, install the wrapper command via shell script to e.g. your local `bin` directory:
```bash
mkdir -pv ~/bin; cp -rv ./texlive-cmd-docker.sh ~/bin
```
and set links for required tex commands
```bash
ln -sv texlive-cmd-docker.sh ~/bin/latex
```

Usage:
-----

Use the wrapper directly (remember to have set your PATH variable!)

```bash
lualatex --version
```

or run a texlive command within the docker container (this is essentially what the wrapper does for you)
```bash
docker run -ti \
         -v ${PWD}:/texsrc \
         -v ${TEXMFHOME}:/texmf \
         -v ${TEXMFCACHE}:/texcache \
         \
         registry.gitlab.com/anzil/docker-texlive \
         \
         /bin/bash -i -c "texhash /texmf && ${CMD} ${ARG}"
```
Here, the current working directory (PWD) is mounted in the container to `/texsrc` and the
user's `texmf` directory (TEXMFHOME) is mounted to `/texmf`.
Typically, TEXMFHOME contains your custom-made tex files, e.g. style and class files.
Read
[here](https://en.wikibooks.org/wiki/LaTeX/Installing_Extra_Packages#Installing_a_package)
and
[here](http://www.math.illinois.edu/~ajh/tex/tips-customstyles.html)
for more information on how the `texmf` directory needs to be organised.
With TEXMFCACHE you can provide the location of your local texlive cache folder,
which can help to speed up compilation runs by re-using font tables etc.

```bash
export TEXMFHOME=${HOME}/.texmf # points to custom tex files in ~/.texmf
export TEXMFCACHE=${HOME}/.texcache # points to local cache folder ~/.texcache
```

On macOS together with homebrew:
-----

Install `basictex` via homebrew

```bash
brew install basictex
```

Install our selection of TeX packages (as sudo):

```bash
sudo tlmgr update --self --all --list
sudo xargs < texlive.package tlmgr install
```

Install your custom tex files (styles, packages, fonts) to a location of your choice (e.g. `~/.texmf`).

Set the custom TeX environment permanently (as sudo):

```bash
sudo tlmgr conf texmf TEXMFHOME "~/.texmf"
sudo tlmgr conf texmf OSFONTDIR "~/.texmf/fonts"
```

