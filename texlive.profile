selected_scheme scheme-minimal

TEXDIR /texlive
TEXMFSYSVAR /texlive/texmf-sysvar
TEXMFSYSCONFIG /texlive/texmf-sysconfig
TEXMFLOCAL /texlive/texmf-local
TEXMFVAR /texlive/texmf-var
TEXMFCONFIG /texlive/texmf-config
TEXMFHOME /texmf

collection-basic 1
collection-langenglish 0
collection-langgerman 0
collection-latex 1
collection-latexrecommended 0
collection-luatex 1
collection-xetex 0

option_autobackup 0
option_doc 0
option_fmt 1
option_letter 0
option_src 0
portable 1
