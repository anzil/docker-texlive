FROM alpine

ARG install_deps="xz lz4 tar ca-certificates"
ARG runtime_deps="perl ncurses git make wget bash python3"

ARG texlive_host="https://ctan.kako-dev.de/systems/texlive/tlnet"

# ------------------------------------------------------------------------------
RUN apk update \
   && \
   apk upgrade \
   && \
   apk --no-cache add --virtual .runtime_deps ${runtime_deps} \
   && \
   apk --no-cache add --virtual .install_deps ${install_deps} \
   && \
   pip3 install pygments
# ------------------------------------------------------------------------------

COPY texlive.profile /tmp/texlive.profile
COPY texlive.package /tmp/texlive.package

RUN mkdir -p /tmp/texlive-install \
   && \
   wget -qO- ${texlive_host}/install-tl-unx.tar.gz | tar xz -C /tmp/ \
   && \
   cd /tmp/install-tl-* \
   && \
   ./install-tl -repository ${texlive_host} -profile /tmp/texlive.profile -force-platform x86_64-linuxmusl

ENV PATH     /texlive/bin/x86_64-linuxmusl:$PATH
ENV MANPATH  /texlive/texmf-dist/doc/man:$MANPATH
ENV INFOPATH /texlive/texmf-dist/doc/info:$INFOPATH

RUN tlmgr init-usertree \
   && \
   tlmgr update --self --all --list \
   && \
   xargs < /tmp/texlive.package tlmgr install \
   && \
   tlmgr conf texmf TEXMFCACHE "/texcache"

VOLUME [ "/texsrc", "/texmf", "/texcache" ]
WORKDIR /texsrc

ENV OSFONTDIR /texmf/fonts

# ------------------------------------------------------------------------------
RUN apk del .install_deps \
   && \
   rm -rf /tmp/* /var/tmp/* /var/cache/apk/*
# ------------------------------------------------------------------------------
