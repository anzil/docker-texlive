# makefile for docker-texlive

TEXMFHOME  ?= ${HOME}/.texmf
TEXMFCACHE ?= ${HOME}/.texcache
TEXBINDIR  ?= ${HOME}/bin

TEXCMDS ?= $(shell cat texlive.command)
WRAPPER := $(patsubst %,$(TEXBINDIR)/%,$(TEXCMDS))

all: install

install: $(WRAPPER) $(TEXMFHOME) $(TEXMFCACHE)

$(TEXBINDIR)/texlive-cmd-docker.sh: texlive-cmd-docker.sh
	mkdir -pv $(TEXBINDIR)
	ln -svf $(realpath texlive-cmd-docker.sh) $(TEXBINDIR)/texlive-cmd-docker.sh

$(WRAPPER): $(TEXBINDIR)/texlive-cmd-docker.sh
	ln -svf texlive-cmd-docker.sh $(@)

$(TEXMFHOME):
	git submodule update --init --recursive
	git submodule update --recursive --remote
	ln -sv $(realpath texmf) $(@)

$(TEXMFCACHE):
	mkdir -pv $(@)

test:
	PATH=$(TEXBINDIR):$${PATH} make -C $(@)

.PHONY: all $(WRAPPER) $(TEXMFHOME) $(TEXMFCACHE) test
